package com.fireball1725.ae2tech.gui.machines;

import appeng.api.config.RedstoneMode;
import appeng.api.config.Settings;
import com.fireball1725.ae2tech.container.machines.ContainerEnergeticIncinerator;
import com.fireball1725.ae2tech.container.machines.ContainerLiquifier;
import com.fireball1725.ae2tech.core.localization.GuiText;
import com.fireball1725.ae2tech.gui.BaseGui;
import com.fireball1725.ae2tech.gui.widgets.GuiImageButton;
import com.fireball1725.ae2tech.gui.widgets.GuiProgressBar;
import com.fireball1725.ae2tech.tileentity.machines.TileEntityEnergeticIncinerator;
import com.fireball1725.ae2tech.tileentity.machines.TileEntityLiquifier;
import net.minecraft.entity.player.InventoryPlayer;

public class GuiLiquifier extends BaseGui {
    GuiProgressBar guiProgressBar;
    GuiProgressBar guiProgressBar2;

    ContainerLiquifier containerLiquifier;
    GuiImageButton redstoneMode;

    public GuiLiquifier(InventoryPlayer inventoryPlayer, TileEntityLiquifier tileEntityLiquifier) {
        super(new ContainerLiquifier(inventoryPlayer, tileEntityLiquifier));
        containerLiquifier = ((ContainerLiquifier) this.inventorySlots);
        this.xSize = 211;
        this.ySize = 152;
    }

    @Override
    public void initGui() {
        super.initGui();
        guiProgressBar = new GuiProgressBar("gui/liquifier.png", this.guiLeft + 105, this.guiTop + 7, 214, 27, 25, 59, GuiProgressBar.Direction.VERTICAL);
        guiProgressBar2 = new GuiProgressBar("gui/liquifier.png", this.guiLeft + 136, this.guiTop + 28, 214, 5, 5, 18, GuiProgressBar.Direction.VERTICAL);

        this.buttonList.add(this.guiProgressBar);
        this.buttonList.add(this.guiProgressBar2);

        addButtons();
    }

    protected void addButtons() {
        this.redstoneMode = new GuiImageButton(this.guiLeft - 18, this.guiTop, Settings.REDSTONE_EMITTER, RedstoneMode.LOW_SIGNAL);
        this.buttonList.add(this.redstoneMode);
    }

    public void drawBG(int offsetX, int offsetY, int mouseX, int mouseY) {
        bindTexture("gui/liquifier.png");
        drawTexturedModalRect(offsetX, offsetY, 0, 0, this.xSize, this.ySize);
    }

    public void drawFG(int offsetX, int offsetY, int mouseX, int mouseY) {
        double progress = (this.containerLiquifier.progress + 1);
        double progress2 = (this.containerLiquifier.progress2 + 1);

        int maxFurnaceCookTime = 10000;
      //  int maxFurnaceCookTime = 150 - ((this.containerLiquifier.upgradeTier + 1) * 25);


        progress = (progress / maxFurnaceCookTime);
        progress = progress * 100;
        this.guiProgressBar.current = (int) progress;

        maxFurnaceCookTime = 150 - ((this.containerLiquifier.upgradeTier + 1) * 25);
        progress2 = (progress2 / maxFurnaceCookTime);
        progress2 = progress2 * 100;
        this.guiProgressBar2.current = (int) progress2;

        this.fontRendererObj.drawString(getGuiDisplayName(GuiText.Liquifier.getLocal()), 8, 8, 4210752);
        this.fontRendererObj.drawString(GuiText.inventory.getLocal(), 8, this.ySize - 96 + 2, 4210752);
    }
}

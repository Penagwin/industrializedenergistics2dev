package com.fireball1725.ae2tech.gui.widgets;

public abstract interface ITooltip {
    public abstract String getMessage();

    public abstract int xPos();
    public abstract int yPos();

    public abstract int getWidth();
    public abstract int getHeight();

    public abstract boolean isVisible();
}

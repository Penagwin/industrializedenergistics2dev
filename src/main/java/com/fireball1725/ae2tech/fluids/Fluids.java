package com.fireball1725.ae2tech.fluids;

import com.fireball1725.ae2tech.creativetab.ModCreativeTabs;
import com.fireball1725.ae2tech.lib.Reference;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Items;
import net.minecraft.item.ItemBucket;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.*;

import java.util.ArrayList;

/**
 * Created by Penagwin on 8/19/2014.
 */
public enum Fluids {
    FLUIDMATTER("matter", "FluidMatter"),;

    public static ArrayList<Fluids> fluids = new ArrayList<Fluids>();
    private static boolean registered = false;

    public String name;
    public String ClassName;

    public ItemBucket bucket;

    Fluids(String name, String fluidName) {
        this.name = name;
        this.ClassName = fluidName;
    }


    public static void registerAll() {
        for (Fluids b : Fluids.values())
            b.register();

        if (registered)
            return;

        registered = true;
    }

    public void register() {
        FluidRegistry.registerFluid(new Fluid(name));
        BlockFluidClassic fluidm = (BlockFluidClassic)getClass(ClassName);

        BucketCustom bucket = new BucketCustom(fluidm);
        bucket.setUnlocalizedName(Reference.MOD_ID + ".bucket." + name).setContainerItem(Items.bucket);
        bucket.setTextureName(Reference.MOD_ID + ":bucket_" + name);
        GameRegistry.registerItem(bucket, ".bucket");
        GameRegistry.registerBlock(fluidm.setCreativeTab(ModCreativeTabs.Machines).setBlockTextureName(Reference.MOD_ID + ":" + name).setBlockName(Reference.MOD_ID + "." + this.ClassName.toLowerCase()), "fluid." + name);

        FluidContainerRegistry.registerFluidContainer(FluidRegistry.getFluidStack(name, FluidContainerRegistry.BUCKET_VOLUME), new ItemStack(bucket), new ItemStack(Items.bucket));
    }

    public Object getClass(String fluidName) {
         Object o = null;
        try {
            Class c = Class.forName("com.fireball1725.ae2tech.fluids.fluid." + fluidName);
             o = c.newInstance();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return o;
    }

}

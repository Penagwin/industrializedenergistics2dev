package com.fireball1725.ae2tech.mobs;

/**
 * Created by Penagwin on 8/24/2014.
 */


import com.fireball1725.ae2tech.mobs.Entity.PenguinEntity;
import com.fireball1725.ae2tech.mobs.Model.PenguinModel;
import com.fireball1725.ae2tech.mobs.Renderer.PenguinRenderer;
import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.registry.EntityRegistry;

import net.minecraft.client.renderer.entity.RenderLiving;




/**
 * Created by Penagwin on 8/19/2014.
 */
public enum Mobs {

//  The First color is the color of the spawn egg's specks.
//  The Second color is the color of the rest of the spawn egg.

//   ENUM    Mob Name          Renderer          Model        Scale     Entity class       color1     color2
    PENGUIN("penguin", new PenguinRenderer(new PenguinModel(), 1.0F), PenguinEntity.class, 0x5f5f5f5, 0xfff),;

    private static boolean registered = false;

    public String name;
    public RenderLiving renderer;
    public Class classEntity;
    public int color1;
    public int color2;


    Mobs(String name,RenderLiving renderer, Class classEntity, int color1, int color2) {
        this.name = name;
        this.renderer = renderer;
        this.classEntity = classEntity;
        this.color1 = color1;
        this.color2 = color2;
    }


// This will only be called by the server. So we don't register the Renderer
    public static void registerAll() {
        for (Mobs b : Mobs.values())
            b.register();

        if (registered)
            return;

        registered = true;
    }

    public void register() {
        EntityRegistry.registerGlobalEntityID(classEntity, name, EntityRegistry.findGlobalUniqueEntityId(), color1, color2);

    }
//


//This will only be called by the client. So we register the Renderer
    public static void registerAllWithRender() {
        for (Mobs b : Mobs.values())
            b.registerWithRender();

        if (registered)
            return;

        registered = true;
    }
    public void registerWithRender() {
        RenderingRegistry.registerEntityRenderingHandler(classEntity, renderer);
        this.register();
    }


}

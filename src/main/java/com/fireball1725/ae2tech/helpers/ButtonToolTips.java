package com.fireball1725.ae2tech.helpers;

import net.minecraft.util.StatCollector;

public enum ButtonToolTips {
    RedstoneMode, TransferDirection,
    AlwaysActive,  ActiveWithoutSignal,  ActiveWithSignal,  ActiveOnPulse,
    TransferToNetwork,  TransferFromNetwork;

    String root;

    private ButtonToolTips() {
        this.root = "gui.tooltips.ae2techaddon";
    }

    private ButtonToolTips(String r) {
        this.root = r;
    }

    public String getUnlocalized() {
        return this.root + "." + toString();
    }

    public String getLocal() {
        return StatCollector.translateToLocal(getUnlocalized());
    }
}

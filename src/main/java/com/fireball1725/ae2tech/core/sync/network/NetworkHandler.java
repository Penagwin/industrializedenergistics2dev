package com.fireball1725.ae2tech.core.sync.network;

import com.fireball1725.ae2tech.core.sync.NetworkPacket;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.network.FMLEventChannel;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import net.minecraft.entity.player.EntityPlayerMP;

public class NetworkHandler {
    public static NetworkHandler instance;
    final FMLEventChannel eventChannel;
    final String myChannelName;
    final IPacketHandler clientHandler;
    final IPacketHandler serverHandler;

    public NetworkHandler(String channelName) {
        FMLCommonHandler.instance().bus().register(this);
        this.eventChannel = NetworkRegistry.INSTANCE.newEventDrivenChannel(this.myChannelName = channelName);
        this.eventChannel.register(this);

        this.clientHandler = createClientSide();
        this.serverHandler = createServerSide();
        instance = this;
    }

    private IPacketHandler createServerSide() {
        try {
            return new NetworkPacketHandler();
        } catch (Throwable e) {}
        return null;
    }

    private IPacketHandler createClientSide() {
        try {
            return new NetworkPacketHandler();
        } catch (Throwable e) {}
        return null;
    }

    @SubscribeEvent
    public void clientPacket(FMLNetworkEvent.ClientCustomPacketEvent ev) {
        if (this.clientHandler != null) {
            this.clientHandler.onPacketData(null, ev.packet, null);
        }
    }
    @SubscribeEvent
    public void serverPacket(FMLNetworkEvent.ClientCustomPacketEvent ev) {
        if (this.serverHandler != null) {
            this.serverHandler.onPacketData(null, ev.packet, null);
        }
        	System.out.println("ARG SERVER RECIED " + ev.packet.payload().readInt());
        
    }
    public String getChannel() {
        return this.myChannelName;
    }

    public void sendToAll(NetworkPacket message) {
        this.eventChannel.sendToAll(message.getProxy());
    }

    public void sendTo(NetworkPacket message, EntityPlayerMP playerMP) {
        this.eventChannel.sendTo(message.getProxy(), playerMP);
    }

    public void sendToAllAround(NetworkPacket message, NetworkRegistry.TargetPoint point) {
        this.eventChannel.sendToAllAround(message.getProxy(), point);
    }

    public void sendToDimension(NetworkPacket message, int dimID) {
        this.eventChannel.sendToDimension(message.getProxy(), dimID);
    }

    public void sendToServer(NetworkPacket message) {
        this.eventChannel.sendToServer(message.getProxy());
    }
}

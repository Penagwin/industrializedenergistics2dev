package com.fireball1725.ae2tech.core.sync.packets;

import appeng.api.storage.data.IAEItemStack;
import com.fireball1725.ae2tech.core.sync.NetworkPacket;
import com.fireball1725.ae2tech.core.sync.network.INetworkInfo;
import com.fireball1725.ae2tech.helpers.InventoryAction;
import com.fireball1725.ae2tech.util.Platform;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;

import java.io.IOException;

public class PacketInventoryAction extends NetworkPacket {
    public final InventoryAction action;
    public final int slot;
    public final IAEItemStack slotItem;

    public PacketInventoryAction(ByteBuf stream) throws IOException {
        this.action = InventoryAction.values()[stream.readInt()];
        this.slot = stream.readInt();
        boolean hasItem = stream.readBoolean();
        if (hasItem) {
            // TODO: Load from stream...
            this.slotItem = null;
        } else {
            this.slotItem = null;
        }
    }

    public PacketInventoryAction(InventoryAction action, int slot, IAEItemStack slotItem) throws IOException {
        if ((Platform.isClient()) && (slotItem != null)) {
            throw new RuntimeException("invalid packet, client cannot post inv actions with stacks.");
        }

        this.action = action;
        this.slot = slot;
        this.slotItem = slotItem;

        ByteBuf data = Unpooled.buffer();

        data.writeInt(getPacketID());
        data.writeInt(action.ordinal());
        data.writeInt(slot);
        if (slotItem == null)
        {
            data.writeBoolean(false);
        }
        else
        {
            data.writeBoolean(true);
            slotItem.writeToPacket(data);
        }
        configureWrite(data);
    }

    public void clientPacketData(INetworkInfo network, NetworkPacket packet, EntityPlayer player)
    {

    }

    public void serverPacketData(INetworkInfo manager, NetworkPacket packet, EntityPlayer player)
    {
        EntityPlayerMP sender = (EntityPlayerMP)player;
//        if ((sender.openContainer instanceof AEBaseContainer))
//        {
//            AEBaseContainer aebc = (AEBaseContainer)sender.openContainer;
//            aebc.doAction(sender, this.action, this.slot);
//        }
    }
}

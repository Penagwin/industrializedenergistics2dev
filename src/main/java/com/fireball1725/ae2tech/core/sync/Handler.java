package com.fireball1725.ae2tech.core.sync;

import java.util.ArrayList;
import java.util.Iterator;

import com.fireball1725.ae2tech.tileentity.machines.TileEntitySyncPlate;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.internal.FMLProxyPacket;
/*	Created by Penagwin
 *	7/27/14
 * 	
 * This is my packet handler
 */
@Sharable
public class Handler extends SimpleChannelInboundHandler<FMLProxyPacket> {
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, FMLProxyPacket packet) throws Exception {
		if (packet.channel().equals("guiButton")) {
			ByteBuf payload = packet.payload();

			int id = payload.readInt();
			if (id == 1) {
				int x = payload.readInt();
				int y = payload.readInt();
				int z = payload.readInt();

				String name = ByteBufUtils.readUTF8String(packet.payload());

				System.out.println("name = " + name);

				ArrayList<?> list = (ArrayList<?>) MinecraftServer.getServer().getConfigurationManager().playerEntityList;
				Iterator<?> iterator = list.iterator();
				EntityPlayerMP player = null;
				while (iterator.hasNext()) {
					player = (EntityPlayerMP) iterator.next();
					if (player.getDisplayName() == name) break;
				}
				TileEntitySyncPlate tile = (TileEntitySyncPlate) player.worldObj.getTileEntity(x, y, z);
				tile.toggleIO();
			}
			else if (id == 2) {
				int x = payload.readInt();
				int y = payload.readInt();
				int z = payload.readInt();

				String name = ByteBufUtils.readUTF8String(packet.payload());

				System.out.println("name = " + name);

				ArrayList<?> list = (ArrayList<?>) MinecraftServer.getServer().getConfigurationManager().playerEntityList;
				Iterator<?> iterator = list.iterator();
				EntityPlayerMP player = null;
				while (iterator.hasNext()) {
					player = (EntityPlayerMP) iterator.next();
					if (player.getDisplayName() == name) break;
				}
				TileEntitySyncPlate tile = (TileEntitySyncPlate) player.worldObj.getTileEntity(x, y, z);
				tile.toggleAllInv();
			}

		}
	}
}
package com.fireball1725.ae2tech.core.sync;

import com.fireball1725.ae2tech.core.sync.network.INetworkInfo;
import com.fireball1725.ae2tech.core.sync.network.NetworkHandler;
import cpw.mods.fml.common.network.internal.FMLProxyPacket;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

import java.nio.ByteBuffer;

public abstract class NetworkPacket {
    private ByteBuf p;
    NetworkPacketHandlerBase.PacketTypes id;

    public final int getPacketID() {
        return NetworkPacketHandlerBase.PacketTypes.getID(getClass()).ordinal();
    }

    public void serverPacketData(INetworkInfo manager, NetworkPacket packet, EntityPlayer player) {
        throw new RuntimeException("packet " + getPacketID() + " does not implement a server side handler");
    }

    public void clientPacketData(INetworkInfo manager, NetworkPacket packet, EntityPlayer player) {
        throw new RuntimeException("packet " + getPacketID() + " does not implement a client side hanlder");
    }

    protected void configureWrite(ByteBuf data) {
        //this.capacity(data.readableBytes());
        this.p = data;
    }

    public FMLProxyPacket getProxy() {
        if (this.p.array().length > 2097152) {
            throw new IllegalArgumentException("Somehow a packet with the length of " + this.p.array().length + " was created by accident...  oops...");
        }
        return new FMLProxyPacket(this.p, NetworkHandler.instance.getChannel());
    }
}

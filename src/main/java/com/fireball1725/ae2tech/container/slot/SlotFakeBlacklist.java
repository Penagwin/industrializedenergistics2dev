package com.fireball1725.ae2tech.container.slot;

import net.minecraft.inventory.IInventory;

public class SlotFakeBlacklist extends SlotFakeTypeOnly {
    public SlotFakeBlacklist(IInventory inv, int idx, int x, int y) {
        super (inv, idx, x, y);
    }

    public boolean renderIconWithItem() {
        return true;
    }

    public float getOpacityOfIcon() {
        return 0.8F;
    }

    public int getIcon() {
        if (getHasStack()) {
            return getStack().stackSize > 0 ? 30 : 14;
        }
        return -1;
    }
}

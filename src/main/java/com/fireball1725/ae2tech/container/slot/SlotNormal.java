package com.fireball1725.ae2tech.container.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;

public class SlotNormal extends AdvSlot {
    public SlotNormal(IInventory iInventory, int idx, int x, int y) {
        super(iInventory, idx, x, y);
    }

    public boolean isItemValid(ItemStack itemStack) {
        if (itemStack == null) {
            return false;
        }

        if (itemStack.getItem() == null) {
            return false;
        }

        if (!this.inventory.isItemValidForSlot(getSlotIndex(), itemStack)) {
            return false;
        }

        return true;
    }
}

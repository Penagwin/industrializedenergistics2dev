package com.fireball1725.ae2tech.tileentity.machines;

import appeng.api.AEApi;
import appeng.api.features.IGrinderEntry;
import appeng.api.implementations.items.IUpgradeModule;
import appeng.api.implementations.tiles.ICrankable;
import appeng.api.networking.GridFlags;
import appeng.api.networking.events.MENetworkChannelChanged;
import appeng.api.networking.events.MENetworkEventSubscribe;
import appeng.api.networking.events.MENetworkPowerStatusChange;
import appeng.api.networking.security.BaseActionSource;
import appeng.api.networking.security.MachineSource;
import appeng.api.util.AECableType;
import appeng.api.util.DimensionalCoord;
import com.fireball1725.ae2tech.blocks.Blocks;
import com.fireball1725.ae2tech.events.TileEventHandler;
import com.fireball1725.ae2tech.events.TileEventType;
import com.fireball1725.ae2tech.helpers.IMachine;
import com.fireball1725.ae2tech.helpers.InventoryOperation;
import com.fireball1725.ae2tech.items.Items;
import com.fireball1725.ae2tech.reference.Settings;
import com.fireball1725.ae2tech.tileentity.TileEntityAEBaseNetworkInventory;
import com.fireball1725.ae2tech.util.InternalInventory;
import com.fireball1725.ae2tech.util.LogHelper;
import com.fireball1725.ae2tech.util.Platform;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.Packet;
import net.minecraft.world.EnumSkyBlock;
import net.minecraftforge.common.util.ForgeDirection;

public class TileEntityEnergeticCrumbler extends TileEntityAEBaseNetworkInventory {
    private static final int[] slotsTop = new int[]{0};
    private static final int[] slotsBottom = new int[]{1};
    private static final int[] slotsSide = new int[]{0, 1};
    public int pulverizerCookTime = 0;
    int machineState = 0;
    private InternalInventory internalInventory = new InternalInventory(this, 8);
    private BaseActionSource mySrc;
    private boolean isActive = false;
    private boolean isPowered = false;
    private boolean isWorking = false;
    private int upgradeTier = 0;
    private boolean performanceUpgrade = false;

    // Notes:
    // 150 Ticks per item
    // 2 AE/t Idle Draw
    // 1 AE/t when smelting

    // Each upgrade (takes 25 ticks off time, also doubles AE/t when smelting
    // upgr speed   chance  idle    working total cost per item
    // 0    150t    110%    2ae/t   1ae/t   150ae per item  <--- Default w/ no upgrades
    // 1    125t    120%    4ae/t   2ae/t   250ae per item
    // 2    100t    130%    6ae/t   4ae/t   400ae per item
    // 3    75t     140%    8ae/t   8ae/t   600ae per item
    // 4    50t     150%    10ae/t  16ae/t  800ae per item
    // 5    25t     160%    12ae/t  32ae/t  800ae per item

    public TileEntityEnergeticCrumbler() {
        this.mySrc = new MachineSource(this);
        this.gridProxy.setFlags(new GridFlags[]{});
        this.gridProxy.setIdlePowerUsage(Settings.MACHINE_CRUMBLER_POWER_IDLE);
        this.gridProxy.setVisualRepresentation(new ItemStack(Blocks.MACHINE_ENERGETICCRUMBLER.block));
        addNewHandler(new invManager());
    }

    @Override
    public IInventory getInternalInventory() {
        return this.internalInventory;
    }

    @MENetworkEventSubscribe
    public void powerRender(MENetworkPowerStatusChange event) {
        this.isActive = this.gridProxy.isActive();
        this.isPowered = this.gridProxy.isPowered();
        updateMachineState();
        updateLight();
    }

    @Override
    public DimensionalCoord getLocation() {
        return new DimensionalCoord(this);
    }

    @Override
    public AECableType getCableConnectionType(ForgeDirection dir) {
        return AECableType.SMART;
    }

    @Override
    public void onChangeInventory(IInventory paramIInventory, int paramInt, InventoryOperation paramInvOperation, ItemStack paramItemStack1, ItemStack paramItemStack2) {
        if (paramInt == -1) {
            return;
        }

        // Slot 0 = Input
        // Slot 1 = Output #1
        // Slot 2 = Output #2
        // Slot 3 = Output #3
        // Slot 4 = Upgrade #1
        // Slot 5 = Upgrade #2

        // We dont care about slot #0, #1, #2, or #3
        if (paramInt < 4) {
            return;
        }

        if (paramItemStack1 != null && paramItemStack1.getItem() != null) {
            if (paramItemStack1.getItem().equals(Items.CARD_MACHINEPERFUPGRADE.item)) {
                this.upgradeTier = 0;
                this.performanceUpgrade = false;
                this.gridProxy.setIdlePowerUsage(Settings.MACHINE_CRUMBLER_POWER_IDLE);
                updateMachineState();
                updateLight();
            }
        }

        if (paramItemStack2 != null && paramItemStack2.getItem() != null) {
            if (paramItemStack2.getItem().equals(Items.CARD_MACHINEPERFUPGRADE.item)) {
                this.upgradeTier = paramItemStack2.getItemDamage();
                this.performanceUpgrade = true;
                double powerUsage = Settings.MACHINE_CRUMBLER_POWER_IDLE + ((upgradeTier + 1) * Settings.PERFORMANCE_UPGRADE_POWER_MULTIPLIER);
                this.gridProxy.setIdlePowerUsage(powerUsage);
                updateMachineState();
                updateLight();
            }
        }
    }

    private void processPulverizer() {
        // TODO: Code in the pulverizer stuff here...
        if (canPulverize()) {
            pulverizeItem();
        }
    }

    private boolean canPulverize() {
        if (this.internalInventory.getStackInSlot(0) == null) {
            return false;
        } else {
            ItemStack itemStack = this.internalInventory.getStackInSlot(0);
            IGrinderEntry r = AEApi.instance().registries().grinder().getRecipeForInput(itemStack);
            if (r != null) {
                if (itemStack.stackSize >= r.getInput().stackSize) {
                    return true;
                }
            }
        }
        return false;
    }

    private void pulverizeItem() {
        ItemStack itemStack = this.internalInventory.getStackInSlot(0);
        IGrinderEntry r = AEApi.instance().registries().grinder().getRecipeForInput(itemStack);
        if (r != null) {
            if (itemStack.stackSize >= r.getInput().stackSize) {
                itemStack.stackSize -= r.getInput().stackSize;
                ItemStack ais = itemStack.copy();
                ais.stackSize = r.getInput().stackSize;
                if (itemStack.stackSize <= 0) {
                    itemStack = null;
                }
                internalInventory.setInventorySlotContents(0, itemStack);
                // ais = item to process...

                IGrinderEntry p = AEApi.instance().registries().grinder().getRecipeForInput(ais);
                if (p != null) {
                    // TODO: stuff here to figure out slots and whatnot...

                    internalInventory.setInventorySlotContents(1, r.getOutput());

                    internalInventory.setInventorySlotContents(2, r.getOptionalOutput());
                }
            }
        }
    }

    public boolean isPowered() {
        return this.isPowered;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public boolean isWorking() {
        return this.isWorking;
    }

    @Override
    public void setInventorySlotContents(int i, ItemStack itemStack) {
        this.internalInventory.setInventorySlotContents(i, itemStack);

        if (itemStack != null && itemStack.stackSize > this.getInventoryStackLimit()) {
            itemStack.stackSize = this.getInventoryStackLimit();
        }
    }

    @Override
    public boolean isItemValidForSlot(int i, ItemStack itemStack) {
        if (i == 0) {
            IGrinderEntry r = AEApi.instance().registries().grinder().getRecipeForInput(itemStack);
            return r != null ? true : false;
        }

        if (i == 1) {
            return false;
        }

        if (i == 2) {
            return false;
        }

        if (i == 3) {
            return false;
        }

        if (itemStack != null) {
            if (itemStack.getItem() instanceof IUpgradeModule) {
                if (itemStack.getItem().equals(AEApi.instance().materials().materialCardRedstone.item())) {
                    if ((internalInventory.getStackInSlot(4) != null && internalInventory.getStackInSlot(4).getItem().equals(AEApi.instance().materials().materialCardRedstone.item())) || (internalInventory.getStackInSlot(5) != null && internalInventory.getStackInSlot(5).getItem().equals(AEApi.instance().materials().materialCardRedstone.item()))) {
                        return false;
                    }

                    return true;
                }
                if (itemStack.getItem().equals(Items.CARD_MACHINEPERFUPGRADE.item)) {
                    if ((internalInventory.getStackInSlot(4) != null && internalInventory.getStackInSlot(4).getItem().equals(Items.CARD_MACHINEPERFUPGRADE.item)) || (internalInventory.getStackInSlot(5) != null && internalInventory.getStackInSlot(5).getItem().equals(Items.CARD_MACHINEPERFUPGRADE.item))) {
                        return false;
                    }

                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean canInsertItem(int i, ItemStack itemStack, int j) {
        return this.isItemValidForSlot(i, itemStack);
    }

    @Override
    public boolean canExtractItem(int i, ItemStack itemStack, int j) {
        return i != 0;
    }

    @Override
    public int[] getAccessibleSlotsFromSide(int side) {
        return side == 0 ? slotsBottom : (side == 1 ? slotsTop : slotsSide);
    }

    public int getState() {
        return machineState;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound) {
        super.readFromNBT(nbtTagCompound);
        int oldMachineState = this.machineState;

        isActive = nbtTagCompound.getBoolean("isActive");
        isPowered = nbtTagCompound.getBoolean("isPowered");
        isWorking = nbtTagCompound.getBoolean("isWorking");
        pulverizerCookTime = nbtTagCompound.getInteger("pulvTime");
        machineState = nbtTagCompound.getInteger("machineState");
        upgradeTier = nbtTagCompound.getInteger("upgradeTier");

        if (oldMachineState != machineState) {
            updateLight();
        }
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound) {
        super.writeToNBT(nbtTagCompound);
        nbtTagCompound.setBoolean("isActive", isActive);
        nbtTagCompound.setBoolean("isPowered", isPowered);
        nbtTagCompound.setBoolean("isWorking", isWorking);
        nbtTagCompound.setInteger("pulvTime", pulverizerCookTime);
        nbtTagCompound.setInteger("machineState", machineState);
        nbtTagCompound.setInteger("upgradeTier", upgradeTier);
    }

    public void updateMachineState() {
        machineState = !isPowered ? 0 : !isWorking ? 1 : 2;

        try {
            worldObj.markBlockForUpdate(xCoord, yCoord, zCoord);
        } catch (Exception e) {
        }
    }

    public void updateLight() {
        try {
            if (worldObj.provider.hasNoSky) {
                worldObj.updateLightByType(EnumSkyBlock.Sky, xCoord, yCoord, zCoord);
            }

            worldObj.updateLightByType(EnumSkyBlock.Block, xCoord, yCoord, zCoord);
        } catch (Exception e) {
        }

    }

    public int getUpgradeTier() {
        return upgradeTier;
    }

    private class invManager extends TileEventHandler {
        public invManager() {
            super(TileEventType.WORLD_NBT, TileEventType.TICK);
        }

        @Override
        public void Tick() {
            processPulverizer();
            super.Tick();
        }
    }
}
